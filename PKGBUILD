# Maintainer: Maxime Gauduin <alucryd@archlinux.org>
# Contributor: Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor: Ionut Biru <ibiru@archlinux.org>
# Contributor: Tom Newsom <Jeepster@gmx.co.uk>
# Contributor: Paul Mattal <paul@archlinux.org>

# ALARM: Kevin Mihelich <kevin@archlinuxarm.org>
#  - use -fPIC in host cflags for v7/v8 to fix print_options.c compile
#  - remove makedepends on ffnvcodec-headers, remove --enable-nvenc, --enable-nvdec
#  - remove depends on aom, remove --enable-libaom
#  - remove depends on intel-media-sdk, remove --enable-libmfx
#  - remove depends on vmaf, remove --enable-vmaf
#  - remove depends on rav1e, remove --enable-librav1e
#  - remove depends on svt-av1, remove --enable-libsvtav1
#  - remove --enable-lto

pkgname=ffmpeg-rk
_pkgname=FFmpeg
_pkgver="4.4.1-Nexus-Alpha1"
pkgver=4.4.1
pkgrel=4
epoch=1
_commit=abbce62231baffe237e412689c71ffe01bfc83135afd375f1e538caae87729ed
pkgdesc='Complete solution to record, convert and stream audio and video'
arch=(aarch64)
url=https://ffmpeg.org/
license=(GPL3)
depends=(
  alsa-lib
  bzip2
  fontconfig
  fribidi
  gmp
  gnutls
  gsm
  jack
  lame
  libass.so
  libavc1394
  libbluray.so
  libbs2b.so
  libdav1d.so
  libdrm
  libfreetype.so
  libgl
  libiec61883
  libmodplug
  libpulse
  libraw1394
  librsvg-2.so
  libsoxr
  libssh
  libtheora
  libva.so
  libva-drm.so
  libva-x11.so
  libvdpau
  libvidstab.so
  libvorbisenc.so
  libvorbis.so
  libvpx.so
  libwebp
  libx11
  libx264.so
  libx265.so
  libxcb
  libxext
  libxml2
  libxv
  libxvidcore.so
  libzimg.so
  opencore-amr
  openjpeg2
  opus
  sdl2
  speex
  ocl-icd
  srt
  v4l-utils
  xz
  zlib
)
makedepends=(
  amf-headers
  avisynthplus
  clang
  git
  ladspa
  nasm
)
optdepends=(
  'avisynthplus: AviSynthPlus support'
  'ladspa: LADSPA filters'
)
provides=(
  ffmpeg
  libavcodec.so
  libavdevice.so
  libavfilter.so
  libavformat.so
  libavutil.so
  libpostproc.so
  libswresample.so
  libswscale.so
)
conflicts=(ffmpeg)
options=(
  debug
)
source=(
  "https://github.com/xbmc/FFmpeg/archive/${_pkgver}.tar.gz"
  'ffmpeg-001-libreelec.patch'
  'ffmpeg-001-v4l2-request.patch'
  'ffmpeg-001-v4l2-drmprime.patch'
  '9ade6ae8b788daf860368ece2bcd6d5aada7ec02.patch'
  '8315f0f522b04abcc003f30b2069dfdf290a4efe.patch'
)
b2sums=('51d310e7000aeba657d55341c5fdb540474e197b85062228ab4b314c8309ec11985aa7f105193333fc6106529e8e58c86eafe268190894be8532d0e0b9065fa6'
        'd0fccf3f9f3131b583ae26a46933683adbe30b928bf2b6e8ff4d1a4b211a3126bfc4d55e7f37c5a37c38000a98de72226134bf3624be6cabb0ce75a88b317b43'
        'd69189fe5bcad59636b49aa2cfcd70ac58d7eae26570342df79591f53399ca0f440a4863e7f41b6324e8c552d2642a020ee3678681abd839e8546d9d08990f4f'
        '2396ab0b8d4d81135a9cbe7da9666c059ce5a72055b1bc458b5a0ec7eae67097487f3f89c14ee4f6ba222baff2218b69cbf6d01aa2c596967c83f0c9abaf493a'
        '962388526becbbf013cee1f3dd7aeff438260947126c3118faf9705975da4959306a0225038c2cb0048c59b34d428d6402a6cfcda07f1e3258195ede7fbbceb5'
        'b0a46f2e001c95a69c34c7ee02e75a5becaeb5f42cc033353fbb3e10feb2e3a259c3db3baf1c2076c334af6c709a7f9e330fcf4318f484b5b5884ea590b606cb')

prepare() {
  cd ${_pkgname}-${_pkgver}
 # git cherry-pick -n 988f2e9eb063db7c1a678729f58aab6eba59a55b # fix nvenc on older gpus
  #patch -Np1 -i ../ffmpeg-vmaf2.x.patch # vmaf 2.x support
  #patch -Np1 -i ../add-av_stream_get_first_dts-for-chromium.patch # https://crbug.com/1251779
  echo "Applying Patch to "${pkgname}""
  echo "Applying LE patch"  
  patch -Np1 -i ../ffmpeg-001-libreelec.patch
  echo "Applying DRMPrime patch"
  patch -Np1 -i ../ffmpeg-001-v4l2-drmprime.patch
  echo "Applying V4l2 request patch"
  patch -Np1 -i ../ffmpeg-001-v4l2-request.patch
  patch -Np1 -i ../8315f0f522b04abcc003f30b2069dfdf290a4efe.patch
  #patch -Np1 -i ../9ade6ae8b788daf860368ece2bcd6d5aada7ec02.patch

}

build() {
  cd ${_pkgname}-${_pkgver}

  [[ $CARCH == "armv7h" || $CARCH == "aarch64" ]] #&& CONFIG='--host-cflags="-fPIC"'

  ./configure \
              --prefix=/usr \
              --disable-debug \
              --enable-static \
              --disable-stripping \
              --enable-amf \
              --enable-avisynth \
              --enable-cuda-llvm \
              --enable-fontconfig \
              --enable-gmp \
              --enable-gnutls \
              --enable-gpl \
              --enable-ladspa \
              --enable-libass \
              --enable-libbluray \
              --enable-libbs2b \
              --enable-libdav1d \
              --enable-libdrm \
              --enable-libudev --enable-v4l2-request \
              --enable-libfreetype \
              --enable-libfribidi \
              --enable-libgsm \
              --enable-libiec61883 \
              --enable-libjack \
              --enable-libmodplug \
              --enable-libmp3lame \
              --enable-libopencore_amrnb \
              --enable-libopencore_amrwb \
              --enable-libopenjpeg \
              --enable-libopus \
              --enable-libpulse \
              --enable-librsvg \
              --enable-libsoxr \
              --enable-libspeex \
              --enable-libsrt \
              --enable-libssh \
              --enable-libtheora \
              --enable-libv4l2 \
              --enable-libvidstab \
              --enable-libvorbis \
              --enable-libvpx \
              --enable-libwebp \
              --enable-libx264 \
              --enable-libx265 \
              --enable-libxcb \
              --enable-libxml2 \
              --enable-libxvid \
              --enable-libzimg \
              --enable-opengl \
              --enable-shared \
              --enable-version3 \
              $CONFIG

  make
  make tools/qt-faststart
  make doc/ff{mpeg,play}.1
}

package() {
  make DESTDIR="${pkgdir}" -C ${_pkgname}-${_pkgver} install install-man
  install -Dm 755 ${_pkgname}-${_pkgver}/tools/qt-faststart "${pkgdir}"/usr/bin/
}

# vim: ts=2 sw=2 et:

